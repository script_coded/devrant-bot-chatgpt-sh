#!/bin/bash


DR_API_URL="https://devrant.com/api"
DR_API_USER=${DR_API_USER:-chatgpt}
DR_API_PASS=${DR_API_PASS:?Missing DR_API_PASS for user ${DR_API_USER}}


dr::login() {
    DR_AUTH_RESP=$(curl -sLk "${DR_API_URL}/users/auth-token" -d "app=3&username=${DR_API_USER}&password=${DR_API_PASS}")
    DR_AUTH_OK=$(jq -j '.success' <<<"${DR_AUTH_RESP}")
    DR_AUTH_TOKEN_ID=$(jq -j '.auth_token.id' <<<"${DR_AUTH_RESP}")
    DR_AUTH_TOKEN_KEY=$(jq -j '.auth_token.key' <<<"${DR_AUTH_RESP}")
    DR_AUTH_TOKEN_EXP=$(jq -j '.auth_token.expire_time' <<<"${DR_AUTH_RESP}")
    DR_AUTH_USER_ID=$(jq -j '.auth_token.user_id' <<<"${DR_AUTH_RESP}")

#    DR_AUTH_ONELINE=$(jq -j '(.success|tostring) + " " + (.auth_token.id|tostring) + " " + (.auth_token.key|tostring) + " " + (.auth_token.expire_time|tostring) + " " + (.auth_token.user_id|tostring)' <<<"${DR_AUTH_RESP}")

    if [ "${DR_AUTH_OK}" = "true" ] ; then return 0; else return 1; fi
}

CR=$'\r'
LF=$'\n'
CRLF="${CR}${LF}"

dr::ensure_session() {
    if [ -z "${DR_AUTH_TOKEN_KEY}" ] ; then
        dr::login || return 1
    fi
}

dr::get_auth_params() {
    _r=
    dr::ensure_session || return ${?}
    _r="app=3&token_id=${DR_AUTH_TOKEN_ID}&token_key=${DR_AUTH_TOKEN_KEY//#/%23}&user_id=${DR_AUTH_USER_ID}"
}

dr::post_comment() {
    local thread_id=${1:?Missing thread/rant ID}
    local content=${2:?Missing comment content}
    local BOUNDARY="----WebKitFormBoundaryted8qdRER2M3AdNK"

    local COMMENT_REQUEST_BODY

    dr::ensure_session

    read -r -d '' COMMENT_REQUEST_BODY <<EOF
--${BOUNDARY}${CRLF}Content-Disposition: form-data; name="app"${CRLF}${CR}
3${CR}
--${BOUNDARY}${CRLF}Content-Disposition: form-data; name="comment"${CRLF}${CR}
${content}${CR}
--${BOUNDARY}${CRLF}Content-Disposition: form-data; name="token_id"${CRLF}${CR}
${DR_AUTH_TOKEN_ID}${CR}
--${BOUNDARY}${CRLF}Content-Disposition: form-data; name="token_key"${CRLF}${CR}
${DR_AUTH_TOKEN_KEY}${CR}
--${BOUNDARY}${CRLF}Content-Disposition: form-data; name="user_id"${CRLF}${CR}
${DR_AUTH_USER_ID}${CR}
--${BOUNDARY}--${CRLF}
EOF
 
    local DR_RESP=$(curl -sLk -X POST "${DR_API_URL}/devrant/rants/${thread_id}/comments" -H "Content-Type: multipart/form-data; boundary=${BOUNDARY}" -d "${COMMENT_REQUEST_BODY}" --compressed)
    local DR_SUCCESS=$(jq -j '.success'<<<"${DR_RESP}")
    _r="${DR_RESP}"
    if [ "${DR_SUCCESS}" = "true" ] ; then return 0; else return 1; fi
}

dr::get_comment() {
    local comment_id=${1:?Comment ID missing. Cannot get comment}
    dr::get_auth_params
    local params=${_r}
    local DR_RESP=$(curl -sLk "${DR_API_URL}/comments/${comment_id}?${params}")
    local DR_SUCCESS=$(jq -j '.success'<<<"${DR_RESP}")
    _r="${DR_RESP}"
    if [ "${DR_SUCCESS}" = "true" ] ; then return 0; else return 1; fi
}

dr::get_rant() {
    local rant_id=${1:?Missing rant ID}
    dr::get_auth_params
    local params=${_r}
    DR_RANT_RESP=$(curl -sLk "${DR_API_URL}/devrant/rants/${rant_id}?app=3&${params}")
    DR_SUCCESS=$(jq -j '.success' <<<"${DR_RANT_RESP}")
    _r="${DR_RANT_RESP}"

    if [ "${DR_SUCCESS}" = "true" ] ; then return 0; else return 1; fi
}

dr::get_rants() {
    DR_RANTS_RESP=$(curl -sLk "${DR_API_URL}/devrant/rants?app=3&limit=20&sort=recent&range=all&skip=0&token_id=&token_key=&user_id=")
    DR_SUCCESS=$(jq '.success' <<<"${DR_RANTS_RESP}")
    _r="${DR_RANTS_RESP}"
    if [ "${DR_SUCCESS}" = "true" ] ; then return 0; else return 1; fi
}

dr::get_notifs() {
    dr::get_auth_params
    local params="${_r}"
    local DR_RESP=$(curl -sLk "${DR_API_URL}/users/me/notif-feed?${params}")
    local DR_SUCCESS=$(jq -j '.success'<<<"${DR_RESP}")
    _r="${DR_RESP}"
    if [ "${DR_SUCCESS}" = "true" ] ; then return 0; else return 1; fi
}

dr::get_notifs_grouped() {
    dr::get_notifs || return ${?}
    local notifs=${_r}
    _r=$(jq -j '.data.items | reduce .[] as $i ({}; .[$i.rant_id|tostring] += [$i])' <<<"${notifs}")
}

## jq 'with_entries(.value=[.value[]|select(.read==1) | select(.type == "comment_mention")] | select(.value|length > 0))'

## jq '[.data.items[] | select(.read == 1) | select(.type == "comment_mention") ] | reduce .[] as $i ({}; .[$i.rant_id|tostring] += [$i])'





