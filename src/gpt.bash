#!/bin/bash
#set -x

GPT_TOKEN=${GPT_TOKEN:?Cannot invoke GPT API - missing GPT_TOKEN}

GPT_URL="https://api.openai.com/v1/chat/completions"
GPT_MODEL="${GPT_MODEL:-gpt-3.5-turbo}"
GPT_ROLE="${GPT_ROLE:-user}"

GPT_TEXT=${GPT_TEXT:-false}
if [ "${1}" = "-q" ] ; then GPT_TEXT=true; shift; fi


NL=$'\n'



gpt::ask() {
    
    GPT_CONTENT=${1:-${GPT_CONTENT:?Missing GPT_CONTENT. What is the question?}}
    GPT_CONTENT=${GPT_CONTENT//\"/}

    read -r -d '' GPT_REQ_BODY <<EOF
{
  "model":"${GPT_MODEL}",
  "messages":[{"role":"${GPT_ROLE}", "content":"${GPT_CONTENT}"}]
}
EOF

    GPT_REQ_BODY=$(jq --arg model ${GPT_MODEL} --arg role "${GPT_ROLE}" --arg content "${GPT_CONTENT}" '{model: $model, messages: [{role: $role, content: $content}]}' <<<"{}")

    GPT_RESP=$(curl -w '%{http_code}' -sLk "${GPT_URL}" -H "Content-Type: application/json" -H "Authorization: Bearer ${GPT_TOKEN}" -d "${GPT_REQ_BODY}")


    GPT_RESP_CODE=${GPT_RESP##*${NL}}
    GPT_RESP_BODY=${GPT_RESP%${NL}*}

    if [ "${GPT_RESP_CODE}" != 200 ] ; then
        _r="Bad code ${GPT_RESP_CODE}. ${GPT_RESP_BODY}"
        return 1
    fi

    if [ "${GPT_TEXT}" = true ] ; then
        _r=$(jq -j '.choices[0].message.content + "\n"' <<<"${GPT_RESP_BODY}")
    else
        _r="${GPT_RESP_BODY}"
    fi
}



#gpt::ask "${1:?Missing question}"
#echo "${_r}"

