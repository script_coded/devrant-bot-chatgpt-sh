#!/bin/bash

if [ "${DEBUG}" = true ] ; then set -x ; fi
COMMENT_LENGTH_LIMIT=1000
COMMENT_POST_PAUSE=10

NL=$'\n'

. devrant.bash
. gpt.bash

KNOWN_BOTS_CSV=$(curl -sLk 'https://raw.githubusercontent.com/C0D4-101/devrant-bots/master/bots.csv')

err() {
    local code=${1}
    local msg=${2}
    echo "[ERROR]: (${code}) ${msg}" >&2
}

warn() {
    local code=${1}
    local msg=${2}
    echo "[WARN]: (${code}) ${msg}" >&2
}

log() {
    local msg=${1}
    echo "[INFO]: ${msg}" >&2
}

die() {
    local code=${1}
    local msg=${2}
    echo "[FATAL]: (${code}) ${msg}" >&2
    exit "${code}"
}

get_mentions() {
    dr::get_notifs_grouped || die $? "Cannot get mentions. ${_r}"
    local notifs=${_r}
    local unread_mentions=$(jq -j 'with_entries(.value=[.value[]|select(.read==0) | select(.type == "comment_mention")] | select(.value|length > 0))' <<<"${notifs}")

    _r=${unread_mentions}
}

is_user_bot() {
    local user=${1:?Username missing. Cannot assert botness}

    while IFS=, read -r uid uname descr url owner junk ; do
        if [ "${user}" = "${uname}" -o "${user}" = "${uid}" ] ; then
            echo "User is a known bot: ${uid},${uname},${descr},${url},${owner}"
            return 0
        fi
    done <<<"${KNOWN_BOTS_CSV#*${NL}}"
    return 1
}

is_invocation_valid() {
    local comment=${1:?Missing invocation. Cannot assert validity}
    local user=$(jq -r '.comment.user_username' <<<"${comment}")
    if is_user_bot "${user}" ; then
        warn 1 "User ${user} is a bot"
        return 1
    fi
}

answer_mention() {
    local rant_id=${1:?Rant ID missing. Cannot answer}
    local comment_id=${2:?Comment ID missing. Cannot answer}
    dr::get_comment "${comment_id}"  || err $? "Cannot get comment ${comment_id}. ${_r}"
    local comment=${_r}

    if ! is_invocation_valid "${comment}" ; then
        local rc=${?}
        err ${rc} "Bot invocation is not valid. Will not proceed. Invocation: ${comment}"
        return ${rc}
    fi

    GPT_TEXT=true gpt::ask "$(jq -j '.comment.body' <<<"${comment}")" || err $? "Cannot get GPT response. ${_r}"
    local gpt_response=${_r}
    local asker_username=$(jq -j '.comment.user_username' <<<"${comment}")
    local prefix="@${asker_username}${NL}"
    local answer_text="${prefix}${gpt_response}"
    local answer_text_length=${#answer_text}

    if [[ "${answer_text_length}" -ge "${COMMENT_LENGTH_LIMIT}" ]] ; then
        local part=1
        local parts=$(( answer_text_length / COMMENT_LENGTH_LIMIT + 1))
        log "Response will have ${parts} parts"
        local offset

        while [[ ${part} -le ${parts} ]] ; do
            log "  posting part ${part}/${parts}"
            dr::post_comment "${rant_id}" "${answer_text:${offset}:${COMMENT_LENGTH_LIMIT}}"
            offset=$(( offset + COMMENT_LENGTH_LIMIT ))
            part=$(( part + 1 ))
            if [[ ${part} -le ${parts} ]] ; then sleep ${COMMENT_POST_PAUSE}; fi
        done
    else
        dr::post_comment "${rant_id}" "${answer_text}"
    fi
}

mark_read_for_rant() {
    local rant_id=${1:?Cannot mark notifs for rant as read -- rant ID missing}
    dr::get_rant "${rant_id}"
}

answer_mentions() {
    get_mentions
    local mentions=${_r}
    local rants_ids=$(jq -r 'to_entries[].key'<<<"${mentions}")
    
    if [ -z "${rants_ids}" ] ; then log "No mentions found"; fi

    for rant_id in ${rants_ids} ; do
        log "Answering mentions in rant ${rant_id}"
        comments_ids=$(jq -r --arg rant_id "${rant_id}" 'to_entries[]|select(.key==$rant_id)|.value[].comment_id'<<<"${mentions}")
        for comment_id in ${comments_ids} ; do
            log "-- mention ${comment_id}"
            answer_mention "${rant_id}" "${comment_id}"
        done
        log "Marking all rant ${rant_id} notifications as read"
        mark_read_for_rant "${rant_id}"
    done
}

main() {
    answer_mentions
}

main

