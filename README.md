A silly project combining [DevRant](devrant.com) and [ChatGPT](https://chat.openai.com/chat). @-mentioning the bot user (@chatgpt) the script (app.bash) will pick up the mention event, forward the question to ChatGPT, receive its response and post that answer as a comment to the DevRant question.

To launch, a few environments are required:

- DR_API_USER (defaults to chatgpt) - a DevRant user name
- DR_API_PASS - a password for the `${DR_API_USER}`
- GPT_TOKEN - a ChatGPT API token. Can be obtained [here](https://platform.openai.com/account/api-keys)

Having all the environment variables set, launch the `app.bash` script.

DevRant API utilized as per the [unofficial docs](https://devrantapi.docs.apiary.io/).


PREREQUISITES:
- bash
- jq
- curl


Runing the script in loop:

```
while :; do date; GPT_TOKEN=sk-sdfGR... DR_API_USER=chatgpt DR_API_PASS="very_secret_password"  ./app.bash ; echo "Pause..." ; sleep 60; done
```

